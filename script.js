// fetch ("https://ajax.test-danit.com/api/json/users", {method: 'GET'})
// .then(response => response.json())
// .then(data => console.log(data));

// fetch ("https://ajax.test-danit.com/api/json/posts", {method: 'GET'})
// .then(response => response.json())
// .then(data => console.log(data));

class Card {
    constructor(postId, title, text, userId, name, email) {
        this.postId = postId
        this.title = title
        this.text = text
        this.userId = userId
        this.name = name
        this.email = email
    }


    render() {
        const card = document.createElement('div')

        card.id = `${this.postId}`
        card.classList.add('card')
        card.innerHTML = `
            <h2 class="cardTitle">${this.title}</h2>
            <p class="cardText">${this.text}</p>
            <p class="cardUserName"><br><b>Name:</b> ${this.name}</p>
            <p class="cardUserMail"><br><b>Email:</b> ${this.email}</p>
            <div class="cardBtn">
            <button onclick="removeCard(${this.postId})"><b>Delete</b></button>
            </div>
        `
        return card
    }
}
function removeCard(postId) {
    fetch(`https://ajax.test-danit.com/api/json/posts/${postId}`, {
        method: 'DELETE',
    })
        .then(response => {
            if (response.ok) {
                document.getElementById(`${postId}`).remove()
            }
        })
}

fetch("https://ajax.test-danit.com/api/json/users")
    .then(resp => resp.json())
    .then(users => {
        fetch("https://ajax.test-danit.com/api/json/posts")
            .then(resp => resp.json())
            .then(posts => {
              
                const cardsWrapper = document.querySelector('#cardsWrapper')
                posts.forEach(post => {
                    const user = users.find(user => user.id === post.userId)
                    const card = new Card(
                        post.id,
                        post.title,
                        post.body,
                        user.id,
                        user.name,
                        user.email
                    )
                    cardsWrapper.append(card.render())
                })
            })
            .catch(err => console.log(err))
    })
    .catch(err => console.log(err))